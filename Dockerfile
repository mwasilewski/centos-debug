FROM centos:latest
RUN yum install -y curl git python bind-utils net-tools httpd wget && \
				yum clean all

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh
CMD ["/run-httpd.sh"]
